<nav id="NavHolder" class="navbar navbar-toggleable-sm navbar-inverse bg-inverse sticky-top container-fluid" >
    <div  class="container" id="ncon" >
        <div id="socialBar" class="social">
            <a class="icon" href="#" target="_blank">
                <img src="<?php echo get_template_directory_uri() . '/img/Hex_Icons _pack/72x72/facebook.png'; ?>">
            </a>
            <a class="icon" href="https://plus.google.com/collection/4ePfNE" target="_blank">
                <img src="<?php echo get_template_directory_uri() . '/img/Hex_Icons _pack/72x72/google plus.png'; ?>">
                <div class="info">
                    <i class="fa fa-google-plus" aria-hidden="true"></i> Agencja Imprez Artystycznych "Roban"
                </div>
            </a>
            <a class="icon" href="https://www.google.pl/maps/place/Ksi%C4%99dza+Andrzeja+Mroczka+75A,+Jaworzno/@50.2165358,19.3429618,17z/data=!3m1!4b1!4m5!3m4!1s0x4716e80f084c727d:0x30f04c47a93d7b8b!8m2!3d50.2165358!4d19.3451505" target="_blank" >
                <img src="<?php echo get_template_directory_uri() . '/img/Hex_Icons _pack/72x72/maps.png'; ?>">
                <div class="info">
                    <i class="fa fa-map-marker" aria-hidden="true"></i> Pokaż w Google Maps
                </div>
            </a>
            <a class="icon" href="mailto:biuro@agencja-imprez.com" info="biuro@agencja-imprez.com" >
                <img src="<?php echo get_template_directory_uri() . '/img/Hex_Icons _pack/72x72/email.png'; ?>">
                <div class="info">
                    <i class="fa fa-envelope-o" aria-hidden="true"></i>  biuro@agencja-imprez.com
                </div>
            </a>
            <a class="icon" href="tel:+48698879415" target="_blank" >
                <img src="<?php echo get_template_directory_uri() . '/img/Hex_Icons _pack/72x72/phone.png'; ?>">
                <div class="info">
                    <i class="fa fa-phone" aria-hidden="true"></i> +48 698-879-415
                </div>
            </a>
            
            <div id="socialInfo" ></div>
        </div>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#MainMenu" aria-controls="MainMenu" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <a class="navbar-brand" href="<?php echo get_home_url(); ?>"><img class="logo" src="<?php echo get_template_directory_uri() . '/img/logo.png'; ?>" ></a>
        <div class="collapse navbar-collapse" id="MainMenu">
            <?php
            wp_nav_menu(array(
                'menu' => 'main-menu',
                'depth' => 2,
                'container' => false,
                'menu_class' => 'nav navbar-nav',
                'walker' => new wp_bootstrap_navwalker())
            );
            ?>  
        </div>

    </div>
</nav>
<script>
    var hideInterval = false;

    function clearInfoInterval() {
        jQuery("#socialInfo").slideUp();
       // jQuery("#socialInfo").hide();
        window.clearInterval(hideInterval);
        hideInterval = false;
    }
    jQuery(function () {
        jQuery("#socialBar .icon").on("mouseover", function () {
            var attr = jQuery(this).children(".info").html();
            if (typeof attr != "undefined" && attr.length > 0) {
                jQuery("#socialInfo").slideDown();
                jQuery("#socialInfo").html(attr);

            }
        });
        jQuery("#socialInfo").on("mouseover",function(){
            if (hideInterval !== false) {
                window.clearInterval(hideInterval);
                hideInterval = false;
            }
        });
        jQuery("#socialInfo").on("mouseout", function () {
            if (hideInterval !== false) {
                window.clearInterval(hideInterval);
                hideInterval = false;
            }
            hideInterval = window.setInterval(clearInfoInterval, 3000);

        });
        jQuery("#socialBar .icon").on("mouseout", function () {
            if (hideInterval !== false) {
                window.clearInterval(hideInterval);
                hideInterval = false;
            }
            hideInterval = window.setInterval(clearInfoInterval, 3000);

        });
    });
</script>