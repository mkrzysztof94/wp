
var debug;
var Albumy = function (data_target) {
    var obj={};
    obj.CustomGalleryEdit={};
     obj.CustomFrame={};
        // the following functions can be used to interact with the custom media uploader

    // input field where the gallery images ids are obj.store as comma separated list
    obj.store = jQuery(data_target);

    // the media uploader
    obj.workflow = false;

    // obj.selection object which list gallery images as collection
    obj.selection={};
    
    obj.customClasses=function() {
       

       
    }.bind(obj);
  
     var media = wp.media;
    var l10n = media.view.l10n;
    this.media=media;
    this.l10n=l10n;
     obj.CustomGalleryEdit = wp.media.controller.GalleryEdit.extend({
            gallerySettings: function (browser) {
                if (!this.get('displaySettings')) {
                    return;
                }

                var library = this.get('library');

                if (!library || !browser) {
                    return;
                }

                browser.toolbar.set('reverse', {
                    text: l10n.reverseOrder,
                    priority: 80,

                    click: function () {
                        library.reset(library.toArray().reverse());
                    }
                });
            }
        });

        obj.CustomFrame = wp.media.view.MediaFrame.Post.extend({
            galleryMenu: function (view) {
            },
            createStates: function () {
                var options = this.options;
                // custom frame has only 2 states: gallery edit/add
                this.states.add([
                    new obj.CustomGalleryEdit({
                        library: options.selection,
                        editing: true,
                        requires: { selection: false},
                        menu: 'gallery'
                    }),
                    new media.controller.GalleryAdd()
                ]);
            },
            galleryEditToolbar: function () {
                try {
                    var updateGallery = l10n.updateGallery;
                    // change the button label
                    l10n.updateGallery = 'Zapisz album';
                    // call parent method
                    media.view.MediaFrame.Post.prototype.galleryEditToolbar.apply(this, arguments);
                    // change the button behaviour so that it would allow us to save an empty gallery
                    this.toolbar.get().options.items.insert.requires.library = false;
                    this.toolbar.get().options.items.insert.click=function(){ obj.update(); jQuery("#post").submit(); };
                    l10n.updateGallery = updateGallery;
                } catch (x) {

                }
            }
        });
    
    obj.getWorkFlow =function(selection) {
        var attributes = {
            state: 'gallery-edit',
            editing: true,
            multiple: true
        };

        if (typeof selection != 'undefined' && selection) {
            attributes.selection = selection;
        }
        return new obj.CustomFrame(attributes);
    }

   obj.init= function() {
        if (window.wp && window.wp.media) {
            obj.customClasses();
        }
       obj.fetch();
    };




    // opens the dialog
    obj.open=function() {
        if (!obj.selection) {
            obj.fetch();
        }

        if (obj.workflow) {
            obj.workflow.off('update', obj.update);
            obj.workflow.dispose();
        }

        obj.workflow = obj.getWorkFlow(obj.selection);
        obj.workflow.on('update', obj.update);
        obj.workflow.open();
       // obj.library=obj.workflow.states.get('gallery-edit');
       // obj.library.attributes.selection.add(jQuery("#album").val().split(','));
    }

    // create obj.selection collection
    obj.fetch =function() {
        var value = obj.store.val();

        if (!value) {
            obj.selection = [];
            return;
        }

        var shortcode = new wp.shortcode({
            tag: "gallery",
            attrs: {ids: value},
            type: "single"
        });
        obj.gallery=wp.media.gallery;
        obj.attachments = obj.gallery.attachments(shortcode);

        obj.selection = new wp.media.model.Selection(obj.attachments.models, {
            props: obj.attachments.props.toJSON(),
            multiple: true
        });

        obj.selection.gallery = obj.attachments.gallery;

        obj.selection.more().done(function () {
            // Break ties with the query.
            obj.selection.props.set({query: false});
            obj.selection.unmirror();
            obj.selection.props.unset("orderby");
        });

    }.bind(obj);
    // retrieve list of gallery images and obj.stores them
    obj.update = function() {
        var library = obj.workflow.states.get('gallery-edit').get('library');
        var ids = library.pluck('id');
        var value = (typeof ids === 'object') ? ids.join(',') : '';
        obj.store.val(value);
        obj.selection = false;
    };
    
    return obj;
}