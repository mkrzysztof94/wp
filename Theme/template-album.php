<?php
/*
 * Template Name: Album
 */
get_header();
?>
<link href="<?php echo get_template_directory_uri() . '/assets/fonts/f1.woff2'; ?>" rel='application/octet-stream' type='text/css'>
<link href="<?php echo get_template_directory_uri() . '/assets/fonts/f2.woff2'; ?>" rel='application/octet-stream' type='text/css'>
<link href="<?php echo get_template_directory_uri() . '/assets/fonts.css'; ?>" rel='stylesheet' type='text/css'>

<?php
$args = array(
    'numberposts' => 20,
    'post_type' => 'albumy',
    'category_name' =>'aktualnosci',
    'exclude' => array("468")
);

include(__DIR__ . "/Elements/StickyMainMenu.php");
?>

<script src="<?php echo get_template_directory_uri() . '/assets/lightbox/js/lightbox.js'; ?>" ></script>
<link href="<?php echo get_template_directory_uri() . '/assets/lightbox/css/lightbox.css'; ?>" rel="stylesheet">

<div id="cont" class="container-fluid">


    <?php
    
    $latest_albums = get_posts($args);
    $output = array();
    $output['album'] = array();
    $output['html'] = "";
    foreach ($latest_albums as &$post) {
//        var_dump($post);
//        echo '<br>';
//        echo '<br>';
//        echo '<br>';
        $post->images = get_post_meta($post->ID, 'img_list', true);
        $post->images = explode(",", $post->images);
        $output['album'][$post->ID] = array("items" => array());
        $output['album'][$post->ID]['data'] = $post;
        $output['album'][$post->ID]['html'] = '<div class="row col-10 album" id="'.$post->ID.'" >' .
                '<div class="opa-c" ></div>' .
                '<div class="album-head">' . $post->post_title . '</div>' .
                '<div class="album-content clearfix">';
        $post->images_size = 0;
        $end = count($post->images) - 1;
        foreach ($post->images as $nr => &$image) {
            $post->images_size++;
            $id = $image;
            $image = (object) wp_get_attachment_metadata($image);
            $image->id = $id;

            foreach ($image->sizes as $k => &$size) {
                $size['url'] = wp_get_attachment_image_url($id, $k);
                if ($k == "thumbnail") {
                    $size['link'] = wp_get_attachment_link($id);
                    $output['album'][$post->ID]['items'][$id] = &$size['link'];
                }
            }
            // var_dump($image->sizes);die;
            $output['album'][$post->ID]['html'] .= '<div class="album-item" title="Kliknij aby powiększyć">' .
                    '<a  href="' . (isset($image->sizes['large']) ? $image->sizes['large']['url']: (isset($image->sizes['full'])?$image->sizes['full']['url']:"")) . '" data-lightbox="' . htmlentities($post->post_title) . '" data-title="" >' .
                    '<img ' . ($nr > 3 ? "lsrc" : "src") . '="' . $image->sizes['medium']['url'] . '" alt="" >' .
                    '</a>' .
                    '</div>';
            if ($nr == 3) {
                $output['album'][$post->ID]['html'] .= '<div id="' . $post->ID . '_hiden-content" class="hidden" >';
            }
        }
        if ($end >= 3) {
            $output['album'][$post->ID]['html'] .= '</div>';
        }
        $output['album'][$post->ID]['html'] .= '</div>' .
                ($end > 3?'<div class="album-foot" >' .
                '<button href="#up" id="'.$post->ID.'_cbtn" onclick="cast(\'' . $post->ID . '_hiden-content\',this)" class="btn btn-inverse d-block w-50 btn-center album-btn" title="Kliknij aby rozwinąć" >Więcej</button>' .
                '</div>':"") .
                '</div>';
        echo $output['album'][$post->ID]['html'];
    }
    ?> 

</div>
<script>
    var chil;
    var hash=window.location.hash.toString();
    hash=hash.replace("#","");
    var pos='';
   var hashEl='';
   
    jQuery(function(){
        
        if(hash.length>0){
            hashEl=document.getElementById(hash.toString());
            if(typeof hashEl ==="object" && hashEl!== null){
                pos=hashEl.offsetTop;
                window.scrollTo(0,(pos+50));
                
                 var el = jQuery("#" + hash.toString()+"_hiden-content").children(".album-item").first().children("a").first().click(); 
                 cast(hash.toString()+"_hiden-content",document.getElementById(hash.toString()+"_cbtn"));
            }
        }
    });
    
    function cast(id, btn) {
        var el = jQuery("#" + id);
        if (el.hasClass("hidden")) {
            el.removeClass("hidden");
            jQuery(btn).text("Zwiń");
            chil = el[0];
            chil = jQuery(chil).children("img").context.children;
            var att;
            if (typeof chil[0] !== "undefined" && typeof chil[0].children[0].children[0].attributes.src === "undefined") {
                for (var item = 0; item < chil.length; item++) {
                    att = document.createAttribute("src");       // Create a "class" attribute
                    att.value = chil[item].children[0].children[0].attributes.item("lsrc").value;
                    chil[item].children[0].children[0].attributes.setNamedItem(att);
               }
            }
            btn.attributes.title.value="Kliknij aby zwinąć";
            jQuery(chil).children("img").slideUp();
        } else {
            el.addClass("hidden");
            jQuery(btn).text("Więcej");
            btn.attributes.title.value="Kliknij aby rozwinąć";
        }
    }
</script>
<?php
get_footer();
?>