<?php
/*
 * Template Name: Kontakt
 */
session_start();
function es($a,$attr=true){
    if(isset($_SESSION['old'][$a]) && $attr===true){
        echo 'value="'. esc_attr($_SESSION['old'][$a]).'"';
    }elseif(isset($_SESSION['old'][$a]) && $attr===false){
        echo _e($_SESSION['old'][$a]);
    }
}
if (!isset($_SESSION['msg'])) {
    $_SESSION['msg'] = false;
}
if (!isset($_SESSION['expirity'])) {
    $_SESSION['expirity'] = false;
}
if (!isset($_SESSION['error'])) {
    $_SESSION['error'] = false;
}
if(!isset($_SESSION['old']) || $_SESSION['old']===NULL){
    $_SESSION["old"]=array();
}
if(isset($_SESSION[session_id()]) ){
    $time=$_SESSION[session_id()]['time'];
    $time+=300;
    if($time < time()){
        unset($_SESSION[session_id()]);
    }
}
if (isset($_POST) && isset($_POST['__message'])) {
    $nonce = wp_verify_nonce($_POST['__message'], 'msg');
    if ($nonce == false) {
        wp_redirect(get_home_url() . '/kontakt');
        exit;
    }
    $error = array();
    if (strlen($_POST['imie_nazwisko']) < 3) {
        $error[] = "Wprowadź poprawne imię i nazwisko !";
    }
    if (strlen($_POST['wiadomosc']) == "0") {
        $error[] = "Wiadomość musi posiadać treść !";
    }
    if (count($error) == 0){
        if(!isset($_SESSION[session_id()])){
            $_SESSION[session_id()]=array();
            $_SESSION[session_id()]['try']=1;
            $_SESSION[session_id()]['time']=time();
            
            $_SESSION['msg'] = "Wiadomość została wysłana.";
            ThemeMail(get_option("email_not"), 'Temat: Wiadomość od ' . $_POST['imie_nazwisko'], $_POST['wiadomosc']);
            $_SESSION['old']=NULL;
        }elseif(isset($_SESSION[session_id()]) && $_SESSION[session_id()]['try'] == '1' ){
             $error[] ="Możesz wysłać jedną wiadomość na 5 minut.";
        }
        
    }
    if (count($error) > 0){
         
      $_SESSION['error']=$error;  
      $_SESSION['old']=$_POST;
    
    }

    wp_redirect(get_home_url() . '/kontakt');
    exit;
    die;

   
  //  die;
}

get_header();
include(__DIR__ . "/Elements/StickyMainMenu.php");
?>


<div class="container clearfix" id="kontakt">
    <?php
    if (isset($_SESSION['msg']) && $_SESSION['msg'] !== false) {
        echo '<div class="alert alert-success">' .
        '<strong>' . $_SESSION['msg'] . '</strong>' .
        '</div>';
        $_SESSION['msg'] = false;
    } 
    if (isset($_SESSION['error']) && $_SESSION['error'] !== false) {
        echo '<div class="alert alert-danger">' .
        '<strong>' . implode("<br>",$_SESSION['error']) . '</strong>' .
        '</div>';
        $_SESSION['error'] = false;
    }
    ?>
    <div class="col-6" style="float:left;">
        <div id="googleMap" style="width:100%;height:400px;"></div>

    </div>
    <div class="col-6" id="koninfo" style="float:left;">
        <div class="col-12 nazwa">           
            <h2>Agencja Imprez Artystycznych</h2>
            <h1>"Roban"</h1>
        </div>
        <div class="col-12 kon-con">
            <div class="kon" >
                <span>Adres</span> 
                <p>43-600 Jaworzno ul. ks. Mroczka 75a</p>
            </div>

            <div class="kon" >
                <span>Telefon</span> 
                <i class="fa fa-phone" aria-hidden="true">
                    <p>+48 698-879-415</p>
                </i> 
            </div>

            <div class="kon" >
                <span>E-mail</span>
                <i class="fa fa-envelope-o" aria-hidden="true">
                    <p>biuro@agencja-imprez.com</p>
                </i>

            </div>

        </div>
    </div>
    <form class="clearfix col-12" style="margin-top: 25px; float:left;" action="" method="POST">
        <div class="col-12 heading">
            <h2>Formularz kontaktowy</h2>
        </div>
        <div class="fcont col-12 clearfix" >

            <div class="form-group col-6" style="float:left; padding-left: 0px;">
                <label for="imie_nazwisko" >Imię oraz nazwisko</label>
                <input class="form-control" id="imie_nazwisko" name="imie_nazwisko" aria-describedby="imie_nazwiskoHelp" <?php echo es("imie_nazwisko"); ?> placeholder="Imię Nazwisko" type="text">
                <small id="imie_nazwiskoHelp" class="form-text text-muted">Uzupełnij polę swoim imieniem oraz nazwiskiem.</small>
            </div>

            <div class="form-group col-6" style="float:left; padding-right: 0px;">
                <label for="kontakt" >Kontakt</label>
                <input class="form-control" id="kontakt" name="kontakt" <?php echo es("kontakt"); ?> aria-describedby="kontaktHelp" placeholder="email lub telefon" type="text">
                <small id="kontaktHelp" class="form-text text-muted">Wpisz email lub numer telefonu do kontaktu.</small>
            </div>

        </div>

        <div class="form-group col-12">
            <label for="wiadomosc" >Wiadomość</label>
            <textarea class="form-control col-12" id="wiadomosc" name="wiadomosc" aria-describedby="wiadomoscHelp" rows="8"><?php echo es("wiadomosc",false); ?></textarea>
            <small id="wiadomoscHelp" class="form-text text-muted">Treść wiadomości.</small>
        </div>

        <?php 
        wp_nonce_field("msg", "__message");
        $_SESSION["old"]=NULL;
        ?>


        <button style="display:block;font-size:22px;margin:auto;" type="submit" name="sub" class="btn btn-success">Wyślij wiadomośc</button>


    </form>
</div>

<script>
    var marker = {};
    var mapProp = {};
    var map = {};
    var coords = {a: '50.216601', b: '19.346424'};
    var infowindow = {};
    var mapLink = 'https://www.google.pl/maps/place/Ksi%C4%99dza+Andrzeja+Mroczka+75A,+Jaworzno/@50.2165358,19.3429618,17z/data=!3m1!4b1!4m5!3m4!1s0x4716e80f084c727d:0x30f04c47a93d7b8b!8m2!3d50.2165358!4d19.3451505';
    function myMap() {
        mapProp = {
            center: new google.maps.LatLng(coords.a, coords.b),
            zoom: 12
        };
        infowindow = new google.maps.InfoWindow({
            content: '<div style="color:black" >43-600 Jaworzno  ul. ks. Mroczka 75a</div>'
        });

        map = new google.maps.Map(document.getElementById("googleMap"), mapProp);
        marker = new google.maps.Marker({position: new google.maps.LatLng(coords.a, coords.b), animation: google.maps.Animation.BOUNCE});
        
        
        marker.setMap(map);
        google.maps.event.addListener(marker, 'click', function () {
            win = window.open(mapLink, '_blank');
            win.focus();
        });
        infowindow.open(map, marker);
        
        google.maps.event.addListenerOnce(infowindow, 'idle', function(){
            
            map.setCenter(infowindow.position);
        });
        

        
    }
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCQIqMh6KZINBWIg8GNuMdalZyYGOh9tgQ&callback=myMap"></script>
<?php
get_footer();
?>