<?php
/*
 * Template Name: Strona Główna
 * Description: 
 */
get_header();

$lista = get_post_meta('468', 'img_list', true);
$lista = explode(',', $lista);
//var_dump($lista);
$indicators = '';
$sliders = '';
$first = 0;
for ($i = 0; $i < count($lista); $i++) {
    $zdj = &$lista[$i];

    $zdj = wp_get_attachment_image($zdj, 'large', "", array("class" => "d-block img-fluid"));

    $indicators .= '<li data-target="#homeSlider" data-slide-to="' . $first . '" ' . ($first === 0 ? 'class="active"' : '') . '></li>';
    $sliders .= '<div class="carousel-item ' . ($first === 0 ? 'active' : '') . '">' .
            $zdj
            . '<div class="container" >' .
            '<div class="carousel-caption d-none d-md-block">' .
            '</div>' .
            '</div>' .
            '</div>';
    $first++;
}
?> 
<div id="homeSlider" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
        <?php echo $indicators; ?>
    </ol>
    <div class="carousel-inner" role="listbox">
        <?php echo $sliders; ?>
    </div>
    <a class="carousel-control-prev" href="#homeSlider" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#homeSlider" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>

<?php
$circles = array();
$circles[1] = wp_get_attachment_image_src('343', 'medium', "", array("class" => "d-block img-fluid"));
$circles[2] = wp_get_attachment_image_src('343', 'medium', "", array("class" => "d-block img-fluid"));
$circles[3] = wp_get_attachment_image_src('343', 'medium', "", array("class" => "d-block img-fluid"));

$ft=array();
$ft[1] = wp_get_attachment_image('343', 'large', "", array("class" => "d-block featurette-image mx-auto img-responsive img-fluid"));
$ft[2] = wp_get_attachment_image('343', 'large', "", array("class" => "d-block featurette-image mx-auto img-responsive img-fluid"));

include(__DIR__ . "/Elements/StickyMainMenu.php");
?>

<!-- Marketing messaging and featurettes
================================================== -->
<!-- Wrap the rest of the page in another container to center all the content. -->

<div class="marketing" id="homeContainer">

    <div class="container-fluid bg-opa" style="padding-top: 30px;">

        <div class="card-deck p10">
            
            <div class="col-lg-3 c-w">
                <img class="rounded-circle d-block w-80 img-fluid img-responsive mx-auto" src="<?php echo $circles[1][0]; ?>" alt="Generic placeholder image" width="180" height="180">
                <h2>Tytuł</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin nec accumsan velit. Ut commodo hendrerit sem vel semper. Cras dui justo, consequat a enim pulvinar, mollis pretium nunc. Mauris eu hendrerit libero. </p>
                <p><a class="btn btn-center w-50 btn-inverse" href="#" role="button">Zobacz więcej      &raquo;</a></p>
            </div>
            
            
            <div class="col-lg-3 c-w">
                <img class="rounded-circle d-block w-80 img-fluid img-responsive mx-auto" src="<?php echo $circles[1][0]; ?>" alt="Generic placeholder image" width="180" height="180">
                <h2>Tytuł</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin nec accumsan velit. Ut commodo hendrerit sem vel semper. Cras dui justo, consequat a enim pulvinar, mollis pretium nunc. Mauris eu hendrerit libero. </p>
                <p><a class="btn btn-center w-50 btn-inverse" href="#" role="button">Zobacz więcej      &raquo;</a></p>
            </div>
            
            
            <div class="col-lg-3 c-w">
                <img class="rounded-circle d-block w-80 img-fluid img-responsive mx-auto" src="<?php echo $circles[1][0]; ?>" alt="Generic placeholder image" width="180" height="180">
                <h2>Tytuł</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin nec accumsan velit. Ut commodo hendrerit sem vel semper. Cras dui justo, consequat a enim pulvinar, mollis pretium nunc. Mauris eu hendrerit libero. </p>
                <p><a class="btn btn-center w-50 btn-inverse" href="#" role="button">Zobacz więcej      &raquo;</a></p>
            </div>
            
            
            <div class="col-lg-3 c-w">
                <img class="rounded-circle d-block w-80 img-fluid img-responsive mx-auto" src="<?php echo $circles[1][0]; ?>" alt="Generic placeholder image" width="180" height="180">
                <h2>Tytuł</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin nec accumsan velit. Ut commodo hendrerit sem vel semper. Cras dui justo, consequat a enim pulvinar, mollis pretium nunc. Mauris eu hendrerit libero. </p>
                <p><a class="btn btn-center w-50 btn-inverse" href="#" role="button">Zobacz więcej      &raquo;</a></p>
            </div>
            
         
    
        </div>
    </div>

    <div class="s" style="height: 45px;"></div>

    <div class="container-fluid p0">

        <div class="col-10 mx-auto">
            <div class="card-deck p10 przedmioty-lista">


                <div class="card col-3 m0">
                     <h3 class="card-header">Nagłówek</h3>
                    <img class="card-img-top img-responsive img-fluid" src="<?php echo $circles[1][0]; ?>" alt="Card image cap">
                    <div class="card-block">
                        <h4 class="card-title">Tytuł</h4>
                        <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin nec accumsan velit. Ut commodo hendrerit sem vel semper. Cras dui justo, consequat a enim pulvinar, mollis pretium nunc. Mauris eu hendrerit libero. </p>
                    </div>
                    <div class="card-footer">
                        <a href="#" class="btn btn-primary d-block mx-auto col-10">Szczegóły</a>
                    </div>
                </div>

                <div class="card col-3 m0">
                     <h3 class="card-header">Nagłówek</h3>
                    <img class="card-img-top img-responsive img-fluid" src="<?php echo $circles[1][0]; ?>" alt="Card image cap">
                    <div class="card-block">
                        <h4 class="card-title">Tytuł</h4>
                        <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin nec accumsan velit. Ut commodo hendrerit sem vel semper. Cras dui justo, consequat a enim pulvinar, mollis pretium nunc. Mauris eu hendrerit libero. </p>
                    </div>
                    <div class="card-footer">
                        <a href="#" class="btn btn-primary d-block mx-auto col-10">Szczegóły</a>
                    </div>
                </div>

                <div class="card col-3 m0">
                     <h3 class="card-header">Nagłówek</h3>
                    <img class="card-img-top img-responsive img-fluid" src="<?php echo $circles[1][0]; ?>" alt="Card image cap">
                    <div class="card-block">
                        <h4 class="card-title">Tytuł</h4>
                        <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin nec accumsan velit. Ut commodo hendrerit sem vel semper. Cras dui justo, consequat a enim pulvinar, mollis pretium nunc. Mauris eu hendrerit libero. </p>
                    </div>
                    <div class="card-footer">
                        <a href="#" class="btn btn-primary d-block mx-auto col-10">Szczegóły</a>
                    </div>
                </div>

                <div class="card col-3 m0">
                     <h3 class="card-header">Nagłówek</h3>
                    <img class="card-img-top img-responsive img-fluid" src="<?php echo $circles[1][0]; ?>" alt="Card image cap">
                    <div class="card-block">
                        <h4 class="card-title">Tytuł</h4>
                        <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin nec accumsan velit. Ut commodo hendrerit sem vel semper. Cras dui justo, consequat a enim pulvinar, mollis pretium nunc. Mauris eu hendrerit libero. </p>
                    </div>
                    <div class="card-footer">
                        <a href="#" class="btn btn-primary d-block mx-auto col-10">Szczegóły</a>
                    </div>
                </div>

                </div>

                

                </div>

            

            </div>

        </div>

    </div>


<hr class="featurette-divider">

<div class="row featurette p-rel">
    <div class="opa-c"></div>
    <div class="col-md-7">
        <h2 class="featurette-heading p10">Nagłówek. </h2>
        <p class="lead p10">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin nec accumsan velit. </p>
    </div>
    <div class="col-md-5">
        <?php echo $ft[1]; ?>
    </div>
</div>

<div class="row featurette">
    <div class="col-md-7 push-md-5">
        <h2 class="featurette-heading p10">Nagłówek <span class="text-muted">przykładowy.</span></h2>
        <p class="lead p10">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin nec accumsan velit. </p>
    </div>
    <div class="col-md-5 pull-md-7">
         <?php echo $ft[1]; ?>
    </div>
</div>

<hr class="featurette-divider">


<footer>
    <a class="btn btn-xs d-block btn-inverse w-25" href="#" style="font-weight: bold; font-size:28px; float: right; background: #262626;"><i class="fa fa-angle-up" aria-hidden="true"></i></a>
</footer>
<?php get_footer(); ?>