<?php

/**
 * @package roban
 * @since roban
 */
include('includes/class/walker.class.php');
include('includes/class/Poczta.class.php');
require(__DIR__ . '/includes/class/ThemeOptions.class.php');
require(__DIR__ . '/includes/class/CustomPostType.class.php');
require(__DIR__ . '/includes/class/Albumy.class.php');
require(__DIR__ . '/includes/class/CustomFields.class.php');
function enqueue_media() {
    if( function_exists( 'wp_enqueue_media' ) ) {
        wp_enqueue_media();
    }
}


function roban_scripts() {

    wp_enqueue_style('bootstrap', get_template_directory_uri() . '/assets/bootstrap/css/bootstrap.min.css');
    wp_enqueue_style('carousel', get_template_directory_uri() . '/assets/bootstrap/css/carousel.css');
    wp_enqueue_style('font-awesome', get_template_directory_uri() . '/assets/fa/css/font-awesome.min.css');
    wp_enqueue_style('style', get_stylesheet_uri());
    wp_enqueue_script('jquery', get_template_directory_uri() . '/assets/jquery.min.js');
    wp_enqueue_script('tether', get_template_directory_uri() . '/assets/bootstrap/js/tether.min.js');
    wp_enqueue_script('bootstrap', get_template_directory_uri() . '/assets/bootstrap/js/bootstrap.min.js');
}

function register_main_menu() {
    register_nav_menu('main-menu', __('Główne Menu')); 
}
$custom_post_types = array();
$custom_post_types["aktualnosci"] = new CustomPostType();
$custom_post_types["aktualnosci"]->labels = array('name' => __('Aktualności'), 'singular_name' => __('Aktualność', 'post type singular name'), 'menu_name' => __('Aktualności', 'admin menu'), 'name_admin_bar' => __('Aktualność', 'add new on admin bar'), 'add_new' => __('Dodaj nową', 'news'), 'add_new_item' => __('Dodaj nową Aktualność'), 'new_item' => __('Nowa Aktualność'), 'edit_item' => __('Edytuj Aktualność'), 'view_item' => __('Zobacz  Aktualność'), 'all_items' => __('Wszystkie Aktualności'));
$custom_post_types["aktualnosci"]->values = array('labels' => &$custom_post_types["aktualnosci"]->labels, 'description' => __(''), 'public' => true, 'publicly_queryable' => true, 'show_ui' => true, 'show_in_menu' => true, 'query_var' => true, 'rewrite' => array('slug' => 'news'), 'capability_type' => 'post', 'has_archive' => true, 'hierarchical' => false, 'menu_position' => 2, 'supports' => array('title', 'editor','thumbnail'), 'menu_icon' => 'dashicons-format-aside');


$custom_post_types["wypozycz"] = new CustomPostType();
$custom_post_types["wypozycz"]->labels = array('name' => __('Wypożycz'), 'singular_name' => __('Wypożycz'), 'menu_name' => __('Wypożycz', 'admin menu'), 'name_admin_bar' => __('Wypożycz', 'add new on admin bar'), 'add_new' => __('Dodaj sprzęt', 'news'), 'add_new_item' => __('Dodaj nowy sprzęt'), 'new_item' => __('Nowy sprzęt'), 'edit_item' => __('Edytuj sprzęt'), 'view_item' => __('Zobacz  sprzęt'), 'all_items' => __('Wszystkie przedmioty'));
$custom_post_types["wypozycz"]->values = array('labels' => &$custom_post_types["wypozycz"]->labels, 'description' => __(''), 'public' => true, 'publicly_queryable' => true, 'show_ui' => true, 'show_in_menu' => true, 'query_var' => true, 'rewrite' => array('slug' => 'wypozycz'), 'capability_type' => 'post', 'has_archive' => true, 'hierarchical' => true, 'menu_position' => 3,'taxonomies'=>array('category'), 'supports' => array('title','editor','thumbnail'), 'menu_icon' => 'dashicons-hammer');

$custom_post_types["albumy"] = new CustomPostType();
$custom_post_types["albumy"]->labels = array('name' => __('Albumy'), 'singular_name' => __('Album'), 'menu_name' => __('Albumy', 'admin menu'), 'name_admin_bar' => __('Albumy', 'add new on admin bar'), 'add_new' => __('Dodaj nowy', 'news'), 'add_new_item' => __('Dodaj nowy Album'), 'new_item' => __('Nowy Album'), 'edit_item' => __('Edytuj Album'), 'view_item' => __('Zobacz  Album'), 'all_items' => __('Wszystkie Albumy'));
$custom_post_types["albumy"]->values = array('labels' => &$custom_post_types["albumy"]->labels, 'description' => __(''), 'public' => true, 'publicly_queryable' => true, 'show_ui' => true, 'show_in_menu' => true, 'query_var' => true, 'rewrite' => array('slug' => 'albumy'), 'capability_type' => 'post', 'has_archive' => true, 'hierarchical' => true, 'menu_position' => 4,'taxonomies'=>array('category'), 'supports' => array('title','thumbnail'), 'menu_icon' => 'dashicons-format-gallery');


$ThemeOptions = new ThemeOptions(array("ThemeOption" => array('email_login' => "Login do serwera", 'email_password' => "hasło", 'email_host' => "Serwer email", 'email_port' => "Port", 'email_not' => "Adres email dla powiadomień")), array(array("page_title" => 'Konfiguracja portalu', "menu_title" => 'Konfiguracja', "capability" => 'administrator', "menu_slug" => 'them_settings', "file" => __DIR__ . '/includes/pages/plugin_options.php', "icon_url" => "", "position" => 25)) );
 $zapisz_galerie=function($post_id){
     
      $meta_value = get_post_meta($post_id, 'img_list',true);
      
      $new_meta_value=( isset($_POST['img_list']) ? $_POST['img_list'] :"");
    if(  strlen($new_meta_value) > 0  && ( $meta_value=="" || $meta_value==false ||  strlen($meta_value) == 0  ) ){  
       add_post_meta($post_id,"img_list",$new_meta_value);
        
    }elseif( isset($new_meta_value) && $new_meta_value != $meta_value ){
        update_post_meta( $post_id,"img_list", $new_meta_value );
        
    }
    
    if( strlen($new_meta_value) == 0 || $new_meta_value == ""){
    
        delete_post_meta( $post_id,"img_list");
     }
     return true;
  };
  
CustomFields::addMetaField("albumy","img_list","Zdjęcia",__DIR__.'/includes/metaBox/AlbumyMeta.php',$zapisz_galerie); 


 $zapisz_galerie_do_postuu=function($post_id){
     
      $meta_value = get_post_meta($post_id, 'news_galeria',true);
      
      $new_meta_value=( isset($_POST['news_galeria']) ? $_POST['news_galeria'] :"");
    if(  strlen($new_meta_value) > 0  && ( $meta_value=="" || $meta_value==false ||  strlen($meta_value) == 0  ) ){  
       add_post_meta($post_id,"news_galeria",$new_meta_value);
        
    }elseif( isset($new_meta_value) && $new_meta_value != $meta_value ){
        update_post_meta( $post_id,"news_galeria", $new_meta_value );
        
    }
    
    if( strlen($new_meta_value) == 0 || $new_meta_value == ""){
    
        delete_post_meta( $post_id,"news_galeria");
     }
     
     return true;
  };

CustomFields::addMetaField("aktualnoci","Album","Zdjęcia",__DIR__.'/includes/metaBox/NewsMeta.php',$zapisz_galerie_do_postuu,"side"); 


 $zapisz_galerie_do_sprzetu=function($post_id){
     
      $meta_value = get_post_meta($post_id, 'wypozycz_galeria',true);
      
      $new_meta_value=( isset($_POST['wypozycz_galeria']) ? $_POST['wypozycz_galeria'] :"");
    if(  strlen($new_meta_value) > 0  && ( $meta_value=="" || $meta_value==false ||  strlen($meta_value) == 0  ) ){  
       add_post_meta($post_id,"wypozycz_galeria",$new_meta_value);
        
    }elseif( isset($new_meta_value) && $new_meta_value != $meta_value ){
        update_post_meta( $post_id,"wypozycz_galeria", $new_meta_value );
        
    }
    
    if( strlen($new_meta_value) == 0 || $new_meta_value == ""){
    
        delete_post_meta( $post_id,"wypozycz_galeria");
     }
     
     return true;
  };
 $zapisz_cene_do_sprzetu=function($post_id){
     
      $meta_value = get_post_meta($post_id, 'wypozycz_cena',true);
      
      $new_meta_value=( isset($_POST['wypozycz_cena']) ? $_POST['wypozycz_cena'] :"");
    if(  strlen($new_meta_value) > 0  && ( $meta_value=="" || $meta_value==false ||  strlen($meta_value) == 0  ) ){  
       add_post_meta($post_id,"wypozycz_cena",$new_meta_value);
        
    }elseif( isset($new_meta_value) && $new_meta_value != $meta_value ){
        update_post_meta( $post_id,"wypozycz_cena", $new_meta_value );
        
    }
    
    if( strlen($new_meta_value) == 0 || $new_meta_value == ""){
    
        delete_post_meta( $post_id,"wypozycz_cena");
     }
     
     return true;
  };

CustomFields::addMetaField("wypoycz","wypozycz_galeria","Album",__DIR__.'/includes/metaBox/WypozyczMeta.php',$zapisz_galerie_do_sprzetu,"side"); 
CustomFields::addMetaField("wypoycz","wypozycz_cena","Cena",__DIR__.'/includes/metaBox/WypozyczCena.php',$zapisz_cene_do_sprzetu,"side"); 


$Albumy=new Albumy();

$theme_support=array('title-tag','post-thumbnails','custom-fields','customize-selective-refresh-widgets','custom-logo','post-formats','page-attributes' );
    
foreach($theme_support as &$sup){
    add_theme_support($sup);
}

if (!function_exists('roban_setup')) {

    function roban_setup() {
        global $custom_post_types,$ThemeOptions,$Albumy,$CustomFields;

        add_action('admin_enqueue_scripts', 'enqueue_media');
         add_image_size( 'admin-list-thumb', 80, 80, true);
        add_action('wp_enqueue_scripts', 'roban_scripts');
        add_action('init', 'register_main_menu');
        CustomFields::Init();
        
        foreach ($custom_post_types as &$cpt) {
            //$cpt->Register();
            add_action('init', [$cpt, "Register"]);
        }
        $ThemeOptions->initialize();
    }
    comments_template('/comments.php', false);
}



add_action('after_setup_theme', 'roban_setup');
