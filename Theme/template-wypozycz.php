<?php
/*
 * Template Name: Wypozycz
 */
if (isset($_POST['AxReq']) && isset($_POST['action'])) {

    if ($_POST['action'] === "items" && isset($_POST['filters'])) {
        $fill = (isset($_POST['filters']) ? $_POST['filters'] : false);
        $model = array(
            'numberposts' => (15),
            'category' => "-1",
            'orderby' => 'date',
            'order' => 'DESC',
            'offset' => 0,
            'post_type' => 'wypoycz');
        if ($fill != "false") {

            $model['numberposts'] = (isset($fill['f_per_page']) && $fill['f_per_page'] != "false" ? (int) $fill['f_per_page'] : $model['numberposts'] );
            $model['offset'] = (isset($fill['f_page']) && $fill['f_page'] != "false" ? ((int) $fill['f_page'] * $model['numberposts']) - $model['numberposts'] : 0);
            $fill['f_sort'] = (isset($fill['fsort']) && $fill['fsort'] != "false" ? explode('2', $fill['fsort']) : NULL);
            $fill['f_per_page'] = (isset($fill['f_per_page']) ? $fill['f_per_page'] : 15);
            if (isset($fill['f_sort']) && is_array($fill['f_sort']) && count($fill['f_sort']) == 2) {
                $model['orderby'] = $fill['f_sort'][0];
                $model['order'] = $fill['f_sort'][1];
            }
            if (isset($fill['f_ser']) && $fill['f_ser'] != "false" && strlen($fill['f_ser']) > 0) {
                $model['s'] = $fill['f_ser'];
            }
            if (isset($fill['f_cat']) && $fill['f_cat'] != "false" && $fill['f_cat'] != "-1") {
                $model['category'] = (int) $fill['f_cat'];
            }
        }
        $response = array();
        $items = array();
        $thumb="";
        $extra = get_posts($model);
        foreach ($extra as &$item) {
            $album = get_post_meta($item->ID, "wypozycz_galeria", true);
            
            if (has_post_thumbnail($album)) {
               // echo '<a href="' . get_permalink($_post->ID) . '" title="' . esc_attr($_post->post_title) . '">';
                $thumb= wp_get_attachment_image_src( get_post_thumbnail_id( $album ), "medium" );//get_the_post_thumbnail(, 'medium');
                $thumb='<img src="'.$thumb[0].'" class="li-thumb-img" >';

            }

            $galeria=explode(",", get_post_meta($album,"img_list",true));
            $galeria_out="";
            $first=true;
            foreach($galeria as &$gal){
                $gal = (object) ["id_zdj"=>$gal,"full_url"=>wp_get_attachment_image_url($gal, "full")];
                if($first){
                $galeria_out.='<a title="Kliknij aby zobaczyć galerię" class="li-lbox" href="'. esc_attr($gal->full_url).'" data-lightbox="'.esc_attr($item->ID).'" data-title="'.esc_attr($item->post_title).'" >'.$thumb.'<div class="li-helper">Kliknij aby otworzyć galerię</div> </a>';
                $first=false;
                }else{
                $galeria_out.='<a style="display:none;" href="'. esc_attr($gal->full_url).'" data-lightbox="'.esc_attr($item->ID).'" data-title="'.esc_attr($item->post_title).'"></a>';
                }
                
            }
            // <a href="https://serwer1740467.home.pl/wp-content/uploads/2017/04/P1140431-1024x576.jpg" data-lightbox="Kalwaria '12" data-title=""><img lsrc="https://serwer1740467.home.pl/wp-content/uploads/2017/04/P1140431-300x169.jpg" alt="" src="https://serwer1740467.home.pl/wp-content/uploads/2017/04/P1140431-300x169.jpg"></a>
           // $album = get_post_meta($album, 'img_list', true);
            $item->thumb=$thumb;
            $items[] = (object) ["title" => $item->post_title,"galeria"=>$galeria, "content" => $item->post_content, "cena" => get_post_meta($item->ID, "wypozycz_cena", true),"thumb"=>$thumb,"galeria_out"=>$galeria_out];
        }

        $response['extra']=$extra;
        $response['items'] = $items;
        $response['items'] = ($response['items'] ? $response['items'] : array());
        $response['length'] = count($response['items']);
        $response['query'] = $model;
        //  var_dump($fil);
        echo json_encode($response);
        die;
    }
}

//------------------------------------------END OF LISTING ----------------------------------------------------------//

$args = array(
    'taxonomy' => 'category',
    'hide_empty' => false,
    'parent' => 6
);
$categorie = get_terms($args);
$cat_select = '<select id="f_cat" ><option value="-1" selected >Wszystkie</option>';
foreach ($categorie as &$cat) {
    $cat_select .= '<option value="' . $cat->term_id . '" >' . $cat->name . '</option>';
}
$cat_select .= '</select>';
//var_dump($_SERVER);

get_header();
include(__DIR__ . "/Elements/StickyMainMenu.php");
?>
<script src="<?php echo get_template_directory_uri() . '/assets/lightbox/js/lightbox.js'; ?>" ></script>
<link href="<?php echo get_template_directory_uri() . '/assets/lightbox/css/lightbox.css'; ?>" rel="stylesheet">
<div id="sprzet_container" class="container clearfix" >
    <div id="list-container" class="col-xs-12" >

    </div>
    <div id="filter" >
        <div class="fin col-sm-3" >
            <?php echo $cat_select; ?>
            <label for="f_cat">Kategoria:</label>
        </div>
        <div class="fin col-sm-2" >
            <select id="f_sort">
                <option value="post_date,DESC" selected="selected" >Najnowsze</option>
                <option value="post_date,ASC">Najstarsze</option>                      
                <option value="post_title,ASC">A-Z</option>
                <option value="post_title,DESC">Z-A</option>
            </select>
            <label for="f_sort">Sortuj</label>
        </div>
        <div class="fin col-sm-2" >    
            <select id="f_per_page">
                <option value="10" selected="selected" >10</option>
            </select>
            <label for="f_per_page">Wpisów na stronę</label>
        </div>
        <div class="fin col-sm-2" >
            <select id="f_page">
                <option value="1" selected="selected" >1</option>
            </select>
            <label for="f_page">Strona</label>
        </div>    

        <div class="fin col-sm-3" >
            <input type="text" placeholder="" id="f_ser" name="f_ser" >
            <label for="f_ser">Szukaj</label>
        </div>
    </div>
    <div id="slist" >

    </div>
</div>

<script>
    var filters = {f_cat: false, f_ser: false, f_page: false};
    var data = {};
    var list_container = document.getElementById("list-container");

    function getFilterData() {
        jQuery("#filter select, #filter input").each(function () {
            filters[this.id] = (this.value.length > 0 ? this.value : false);
        });
    }
    function bindEvents() {
        jQuery("#filter select").each(function () {
            this.oninput = update;
        });
        jQuery("#filter input").each(function () {
            this.oninput = update;
        });
    }
    function getItemList() {
        jQuery.post("", {filters: filters, AxReq: true, action: "items"}, renderResponse);
    }
    function renderResponse($e) {
        data = JSON.parse($e);
        console.log(data);
        list_container.innerHTML = "";
        var item = {};
        var citem = {};
        for (var i = 0; i < data.items.length; i++) {
            citem = data.items[i];
            item = {};
            item.cont = document.createElement("div");
            item.cont.classList.add("col-sm-12");
            item.cont.classList.add("list-item");
            item.descCont=document.createElement("div");
            item.descCont.classList.add("col-sm-8");
            item.descCont.classList.add("li-desc");
            
            item.thumb=document.createElement("div");
            item.thumb.innerHTML=citem.galeria_out;
            item.thumb.classList.add("col-sm-4");
            item.thumb.classList.add("li-thumb");
            
            item.head = document.createElement("div");
            item.head.classList.add("head");

            item.head.innerText = citem.title;
            item.desc = document.createElement("div");
            item.desc.innerHTML = citem.content;
            item.desc.classList.add("desc");
            item.cena = document.createElement("div");
            item.cena.classList.add("cena");

            item.cena.innerHTML = "<span>Cena: </span>"+"<strong>"+citem.cena+" zł</strong>";

            
            item.descCont.append(item.head);
            item.descCont.append(item.desc);
            item.descCont.append(item.cena);
            
            item.cont.append(item.thumb);
            item.cont.append(item.descCont);
            list_container.appendChild(item.cont);
            console.log(citem);

        }
    }
    function update() {
        getFilterData();
        getItemList();
    }
    jQuery(function () {
        bindEvents();
        update();
    });
</script>
<?php
get_footer();
?>