<?php

function ThemeMail($adresat,$temat,$tresc,$debug=false){
require_once(__DIR__.'/PHPMailer/PHPMailerAutoload.php'); 

$data=array();
$data['email_host']=(get_option('email_host'));
$data['email_login']=(get_option('email_login'));
$data['email_password']=(get_option('email_password'));
$data['email_port']=(get_option('email_port'));

$mail = new PHPMailer;
$mail->CharSet = "UTF-8";
$mail->Mailer = "smtp"; 
$mail->IsSMTP();

$mail->SMTPAuth = true;
$mail->SMTPSecure = 'tls';
$mail->SMTPOptions = array(
    'ssl' => array(
        'verify_peer' => false,
        'verify_peer_name' => false,
        'allow_self_signed' => true
    )
);
$mail->Host = get_option('email_host');
$mail->SMTPDebug = ($debug != false?1:0);
$mail->Port =get_option('email_port');
$mail->Username = get_option('email_login');
$mail->Password = get_option('email_password');
$mail->SetFrom(get_option('email_login')); 
$mail->FromName = get_option('email_login'); 
$mail->AddAddress($adresat); 

$mail->IsHTML(true); 

$mail->Subject = $temat;
$mail->Body = $tresc;

if(!$mail->Send()&& $debug === true) {
	echo '<br>Wystąpił błąd...<br>';
	echo 'Mailer Error: ' . $mail->ErrorInfo;
}

return $mail;

}