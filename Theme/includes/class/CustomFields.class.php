<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CustomField
 *
 * @author Roz
 */
 class CustomFields{
    public static $fields=[];
    
    public static function addMetaField($target,$id,$name,$metaBox,&$save_callback,$context="advanced"){
        self::$fields[count(self::$fields)]=(object) array("for"=>$target, "Id"=>$id,"Name"=>$name,"MetaBox"=>$metaBox,"Save"=>$save_callback,"context"=>$context );
    }
     public static function Init(){ 
        
        foreach (self::$fields as &$f){
            add_action( 'add_meta_boxes',function()use(&$f){ 
                 add_meta_box( $f->Id, __($f->Name), function()use(&$f){ return include($f->MetaBox); } , $f->for,$f->context); 
             });
             add_action('save_post',$f->Save);  
        }
        
    }
}
