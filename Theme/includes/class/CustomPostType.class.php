<?php
class CustomPostType {
    public $labels;
    public $values;
    
    function __construct($lab=array(),$val=array()) {
        $this->labels=array('name'=> "",'singular_name'=> "",'menu_name'=> "",'name_admin_bar'=> "",'add_new'=> "",'add_new_item'=> "",'new_item'=> "",'edit_item'=> "",'view_item'=> "",'all_items'=> "");
        $this->labels=array_merge($this->labels,$lab);
        $this->values= array('labels'=> '','description' => "",'public'=> true,'publicly_queryable' => true,'show_ui' => true,'show_in_menu'=> true,'query_var'=> true,'rewrite'=> true,'capability_type'=> 'post','has_archive'=> true,'hierarchical'=> true,'menu_position'=> 5,'supports'=> array(), 'menu_icon' => 'dashicons-format-gallery');
        $this->values=array_merge($this->values,$val);
        $this->values['labels']=$this->labels;
        
    }
    public function Register(){
        $this->values['labels']=$this->labels;
        register_post_type( $this->labels['name'], $this->values );
    }
    public function setDescription($a){
        $this->values['description']=__($a);
        return $this;
    }
    public function setPublic($a){
        $this->values['public']=$a;
        return $this;
    }
    public function setPublicQueryable($a){
        $this->values['publicly_queryable']=$a;
        return $this;
    }
    public function setShowUI($a){
        $this->values['show_ui']=$a;
        return $this;
    }
    public function setShowInMenu($a){
        $this->values['show_in_menu']=$a;
        return $this;
    }
    public function setQueryVar($a){
        $this->values['query_var']=$a;
        return $this;
    }
    public function setRewrite($a){
        $this->values['rewrite']=$a;
        return $this;
    }
    public function setHasArchive($a){
        $this->values['has_archive']=$a;
        return $this;
    }
    public function setHierarchical($a){
        $this->values['hierarchical']=$a;
        return $this;
    }
    public function setMenuPosition($a){
        $this->values['menu_position']=$a;
        return $this;
    }
    public function setSupports($a){
        $this->values['supports']=$a;
        return $this;
    }
    public function setMenuIcon($a){
        $this->values['menu_icon']=$a;
        return $this;
    }
    public function setCapabilityType($a){
        $this->values['capability_type']=$a;
        return $this;
    }
    public function labelSetName($nam){
        $this->labels['name']=__($nam);
        return $this;
    }
    public function labelSetSingularName($nam){
        $this->labels['singular_name']=__($nam);
        return $this;
    }
    public function labelSetMenuName($nam){
        $this->labels['menu_name']=__($nam);
        return $this;
    }
    public function labelSetNameAdminBar($nam){
        $this->labels['name_admin_bar']=__($nam);
        return $this;
    }
    public function labelSetAddNew($nam){
        $this->labels['add_new']=__($nam);
        return $this;
    }
    public function labelSetAddNewItem($nam){
        $this->labels['add_new_item']=__($nam);
        return $this;
    }
    public function labelSetNewItem($nam){
        $this->labels['new_item']=__($nam);
        return $this;
    }
    public function labelSetEditItem($nam){
        $this->labels['edit_item']=__($nam);
        return $this;
    }
    public function labelSetViewItem($nam){
        $this->labels['view_item']=__($nam);
        return $this;
    }
    public function labelSetAllItems($nam){
        $this->labels['all_items']=__($nam);
        return $this;
    }

}
