<?php

class ThemeOptions{
	
	protected $options;
	protected $pages;
	protected $menu_pages;
	protected $menu_page;
	protected static $OptionsArray;
	public function __construct($options=array(),$menu_pages=array(),$pages=array()){
		$this->options=$options;
		$this->pages=$pages;
		$this->menu_page=array("page_title"=>"", "menu_title", "capability"=>'administrator',"menu_slug","function"=>'', "icon_url"=>'',"position"=>null);
		$this->menu_pages=$menu_pages;
		self::$OptionsArray=&$this->options;
	}	
	public static function addOption($k,$v,$group_name=false){
            if($group_name===false){
		self::$OptionsArray[$k]=$v;
            }else{
                self::$OptionsArray[$group_name][$k]=$v;
            }    
	}
        public static function addOptionsGroup($group_name,$opts=array()){
		self::$OptionsArray[$group_name]=$opts; 
	}
	public static function get(){
		return self::$OptionsArray;
	}
	
	public function  register_settings() {
		foreach($this->options as $k=> &$opt){
				foreach($opt as $key=> $o){
					register_setting($k,$key);
				}
		}	
	}
	
	function create_menu() {
		$options=&$this->options;
		foreach($this->menu_pages as &$menu_page){
			$menu_page = array_merge($this->menu_page,$menu_page);
			add_menu_page($menu_page['page_title'],
					$menu_page['menu_title'],
					$menu_page['capability'],
					$menu_page['menu_slug'],
					function()use($options,$menu_page){ include($menu_page['file']); },
					$menu_page['icon_url'],
					$menu_page['position'] );	
			
		}
	
	}
	
	function settings_page() {
		include(__DIR__.'/../pages/plugin_options.php');
	} 
	
	public function initialize(){
		if ( is_admin() ){ 
			add_action( 'admin_init', array($this,'register_settings'));
			add_action( 'admin_menu', array($this,'create_menu'));
			
		} else {

		}
	}
	

}