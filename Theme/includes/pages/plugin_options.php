
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/bootstrap/css/bootstrap-theme.min.css">
<div class="wrap">
<div class="col-xs-6">
<h1>Konfiguracja</h1>
<form method="post" action="options.php">
    <table class="form-table">
	<?php 
	
        
	foreach($options as $k=>$option){
			settings_fields($k);
			do_settings_sections($k);
			
				foreach($option as $key => $o){
				?>
					<tr valign="top">
					<th scope="row"><?php echo $o; ?></th>
					<td><input class="form-control" type="text" name="<?php echo $key; ?>" value="<?php echo ($key!="email_password"?esc_attr( get_option($key) ):""); ?>" /></td>
					</tr>	
				<?php
				}
	};
?>

        
    </table>
    
    <?php submit_button(); ?>

</form>
</div>
<div class="col-xs-6">
<h1>Testuj</h1>
<div class="col-xs-12"> 
	<h1>Email</h1>
	<div class="col-xs-4">Wyślij email testowy na adres:</div>
        <form action="" method="POST" >
       
            <div class="col-xs-4"><input class="form-control" name="debug_mail" type="text"> </div>
	<div class="col-xs-4"><button type="submit" class="btn btn-info">Wyślij</button></div>
         </form>
        <div class="Debug">
            <?php 
            if( isset($_POST['debug_mail']) ){
                $mail=ThemeMail($_POST['debug_mail'],'Temat: Test',' Treść testowa');
                var_dump($mail->error);
            }
            ?>
        </div>
</div>
</div> 
</div>