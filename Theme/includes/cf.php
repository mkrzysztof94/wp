<?php
function wpdocs_register_meta_boxes() {
    add_meta_box( 'meta-box-id', __( 'Album'), 'wpdocs_my_display_callback', 'news','side' );
    add_meta_box( 'meta-box-id', __( 'Album'), 'wpdocs_my_display_callback2', 'albums');
}

function wpdocs_my_display_callback2($meta_id){ 

$album_items=get_post_meta($meta_id->ID,"album_items",true);

?>
	<div id="albumy_meta" style="position:relative;height:600px;" ></div>
<script>
var f ;
var par;
var medfr;
var sel=<?php echo json_encode($album_items)?>; 
var r=0;
function sels(){

			if(document.querySelector('li[data-id]')=== null && r < 100){
				r++;
				return setTimeout(sels,100);
				
			}else{
				
				for(var i=0; i < sel.length;i++){
				var que='li[data-id="'+sel[i]+'"]';
				jQuery(que).addClass("selected");
				jQuery(que).parent().prepend(jQuery(que));
				}
				jQuery(".thumbnail").click(function (e){
					e.stopPropagation();
					console.log(e);
					var t=e.currentTarget.parentElement.parentElement.classList;
					if(t.contains("selected")){
						t.remove("selected");
					}else{
						t.add("selected");
					}
					
				});
			}
		
}
jQuery(function(){

f= wp.media({multiple:true});
par=document.getElementById('albumy_meta');
f.open();
par.appendChild(f.el.parentElement.parentElement.parentElement);
jQuery("body").removeClass("modal-open");


jQuery("#publish").click(function(e){
	var i = document.getElementsByClassName('attachment selected');
	if(document.querySelector(".uploading.attachment") !== null){
		e.preventDefault();
		alert("Pliki nie skończyły się wgrywać!. \n Rudzinko spokojnie !");
	}
	for(var v = 0 ; v < i.length ; v++){
		jQuery("#post").append('<input type="hidden" name="album_item_id[]" value="'+i[v].attributes['data-id'].value+'">');
	}
});
		setTimeout(sels,200);
	
});
</script>
<style>
#albumy_meta .media-modal-backdrop{
	position:relative;
	display:none;
}
#albumy_meta .media-modal-content{
	box-shadow:none;
}
#albumy_meta .media-modal {
	position: absolute;
	width: 100%;
	left: 0;
	right: 0;
	top: 0;
	height: 100%;
}
#albumy_meta .button-link.media-modal-close{
	display:none;
}
#albumy_meta > div {
	position:relative;
	width: 100%;
	height: 100%;
	z-index:0;
	}
#albumy_meta .media-frame-router{
	top:0px;
}
#albumy_meta .media-frame-content{
	top:35px;
}
#albumy_meta .media-frame-toolbar{
	display:none;
}
#albumy_meta .media-sidebar{
	display:none;
}
#albumy_meta .media-frame-content{
	bottom:0;
	right:0;
}
#albumy_meta ul.attachments {
	right:0px;
}
</style>
<?php
}
function wpdocs_my_display_callback($meta_id){ 
	global $wpdb,$post;
	$vars=array('posts_per_page' => 10,
    'order'          => 'DESC',
    'orderby'        => 'date',
	'post_type'      => 'albums',
    'posts_per_page' => 25);
	$albums=get_posts($vars);

	$album_id=get_post_meta($meta_id->ID,"album_id",true);
	//var_dump($albums);
	?>

<select name="album_id">
	<?php foreach($albums as $a ) {
	echo '<option value="'.$a->ID.'" '.($a->ID==$album_id?'selected="selected"':"").'>'.$a->post_title.'</option>';
	} ?>
</select>
	
<?php }


	function save_album($post){
		global $post;
		if(isset($post)&&isset($_POST["album_id"])){
			update_post_meta($post->ID, "album_id", (isset($_POST["album_id"]) ? $_POST["album_id"] :0));
		}
		if(isset($post)){
			update_post_meta($post->ID, "album_items", (isset($_POST["album_item_id"]) ? $_POST["album_item_id"] :array()));
		}
	}
	
	add_action( 'add_meta_boxes', 'wpdocs_register_meta_boxes' );
	add_action('save_post', 'save_album');  
