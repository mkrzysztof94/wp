<?php 
$lista=get_post_meta(get_the_ID(),'img_list', true);
//var_dump($lista); 
?>

<input type="hidden" name="img_list" id="album" value="<?php echo $lista; ?>" > 
<div id="gal" style="width: 100%; height:auto;"  ></div>
<script src="<?php echo get_template_directory_uri() . '/assets/jquery.min.js';?>" ></script>
<script src="<?php echo get_template_directory_uri() . '/assets/Albumy.js';?>" ></script>
<link href="<?php echo get_template_directory_uri() . '/assets/bootstrap/css/bootstrap.min.css';?>" rel="stylesheet">

<script>
   var album; 
    jQuery(function(){
        album= Albumy("#album");
        album.init();
        album.open();
        jQuery("#gal").append(album.workflow.$el);
        album.workflow.modal.$el.hide();
        jQuery("#post").submit(function(){ album.update(); });

   });
</script>
<style>
    #advanced-sortables{
        display: block;
        position: relative;

    }
    body.modal-open {
	overflow: auto;
    }
    #gal{
        position: relative;
       
    }
    .media-frame-content {
        position: relative;
        width: 100% !important;
        height: auto !important;
        left: auto;
        right: auto;
        top: 0;

    }
    
    .media-selection{
        display: none;
    }
    .attachment-info{
        width: 30%;
        float: left;

    }
    .attachment-details .setting, .media-sidebar .setting{
        width: 70%;
    }
    .media-menu{
        padding: 3px;
    }
    .media-menu-item{
	color: #fff !important;
	background-color: #0275d8;
        background: #0275d8;
	border-color: #0275d8;

    }
    .media-menu-item:hover, .media-menu-item.active{
        color: #fff;
        background-color: #025aa5 !important;
        background: #025aa5 !important;
        background-image: none;
        border-color: #01549b !important;
        height: 28px !important;
        margin:0 !important;

    }
    .media-router > .media-menu-item.active, .media-router > .media-menu-item:hover{
        height:32px  !important;
    }
    .media-menu-item{
        height: auto;
    }
    .media-router > a{
        height: 32px;
    }
    .media-menu a{
        width: auto !important;
        display: block;
        max-width: 25%;
        font-size: 12px;
        float: left;
        padding: 5px;

    }
    .media-menu .separator{
        display: none;
    }
    .media-toolbar{
        width: 100%;
    }
    .attachments-browser .attachments{
        width: auto;
        height: 100%;
        position: relative;
        display: block;
        margin: 0;
        overflow-x: scroll;
        width: auto;
        height: 350px;
        margin-bottom: 50px;


    }
    .upload-ui{
        height:320px;
    }
    .attachment{
        height: 90px !important;
        display: block;
        float: left;
        width: 90px !important;
        position: relative;
        display: block;
        height: auto;
        z-index: 22;
        margin-bottom: 30px;

    }
    .attachment > input{
        z-index: 22;
    }
    .media-frame.mode-select.wp-core-ui{
        position: relative;
        
    }
    .wp-core-ui .attachment{
        margin: 15px 0;
    }
    .media-frame-toolbar{
        width: 100%;
        position: relative;
        display: block;
        float: left;
        left: 0;

    }
    .media-menu-item ,.media-menu-item.active {
        height: auto;
    }
    .media-sidebar{
        position: relative;
        display: block;
        height: 250px;
        overflow-y: scroll;
        width: 100%;
        padding: 10px;
        resize: vertical;

    }
    .media-frame.hide-router .media-frame-content{
        top:0;
    }
    .media-frame .uploader-inline {
        margin-bottom: 20px;
        padding: 0;
        text-align: center;
        height: auto;
        right: 0;
        position: relative;
        display: block;
    }
    .media-toolbar-primary .button{
        margin: 10px;
    }
    .uploader-inline{
        width: 100%;
        height: 200px;
    }
    .media-frame-menu{
        width: 100%;
    display: block;
    height: 36px;
    position: relative;

    }
    .media-frame-title{
        display: none;
    }
    .media-frame-router{
        position: relative;
        width: 100%;
        left: 0;
        top: auto;
        margin-top: 10px;


    }
    
    .media-frame-content{
        min-height: 350px ;
    }
    
    .media-frame .moxie-shim.moxie-shim-html5{
        height:100%!important; 
        width: 100% !important;
        left: 0 !important;
        right: 18px !important;

    }
    .media-frame .moxie-shim.moxie-shim-html5 > input{
        font-size: 999px;
        opacity: 1;
        position: absolute;
        top: 0 !important;
        left: 0 !important;
        width: 100% !important;
        min-height: 550px !important;
        background: red;
        right: 0 !important;
        bottom: 0 !important;

    }
/*    .uploader-window,.uploader-window-content{
        display: block;
        opacity: 1;
        position: absolute;
        width: 50%;
        height: 50%;
    }*/
    
</style>