<?php
$args = array(
  'numberposts' => -1,
  'post_type'   => 'albumy',
  'tax_query' => [
        [
            'taxonomy' => 'category',
//            'terms' => 13,
            'include_children' => true // Remove if you need posts from term 7 child terms
        ],
    ],
);

$cena = get_post_meta(get_the_ID(), "wypozycz_cena",true) ;

?>

<input type="text" name="wypozycz_cena" value="<?php echo $cena; ?>" >