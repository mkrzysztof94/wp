<?php
$kategoria= wp_get_post_categories(get_the_ID()) ;
$kategoria = (isset($kategoria[0])?$kategoria:6);
$args = array(
  'numberposts' => -1,
  'post_type'   => 'albumy',
  'tax_query' => [
        [
            'taxonomy' => 'category',
            'terms' => $kategoria,
            'include_children' => true // Remove if you need posts from term 7 child terms
        ],
    ],
);

$albumy = get_posts( $args );
$albumc= get_post_meta(get_the_ID(), "wypozycz_galeria",true) ;

$select_options="";
foreach($albumy as &$album){
    $select_options.="<option value=\"{$album->ID}\" ".($album->ID==$albumc?'selected="selected"':"").">{$album->post_title}</option>";
//    var_dump($album);
//    echo '<br>';
//    echo '<br>';
//    echo '<br>';
}
//var_dump($kategoria);
?>

<select name="wypozycz_galeria" id="wypozycz_galeria">
    <?php echo $select_options; ?>
</select>