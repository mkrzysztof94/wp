<?php
/**
 * @since roban
 */

get_header(); 
include(__DIR__ . "/Elements/StickyMainMenu.php"); ?>

<div class="container">

		<?php
		// Start the loop.
		while ( have_posts() ) : the_post();

			// Include the page content template.
			get_template_part( 'content', 'page' );

		// End the loop.
		endwhile;
		?>
</div>

<style>
#sidebar{
	width:20px;
	display:inline-block;
	
}
</style>
<?php get_footer(); ?>
