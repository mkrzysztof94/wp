<?php
/*
 * Template Name: Aktualnosci
 */
$args = array(
  'numberposts' => 15,
  'post_type'   => 'aktualnoci'
);

$aktualnosci = get_posts($args);
$out='';
$l_r=true;
foreach($aktualnosci as &$a){
    $a->album=get_post_meta($a->ID,'news_galeria', true);
    $a->thumb=get_the_post_thumbnail_url($a->ID,"large");
//    var_dump($a);
//    echo "<br>";
//    echo "<br>";
//    echo "<br>";
    if($l_r){
    $out.='<div class="row featurette p-rel news" >'.
                '<div class="col-md-8">'.
                    '<h2 class="featurette-heading p10">'.$a->post_title.' </h2>'.
                    '<p class="lead p10">'.$a->post_content.'</p>'.
                '</div>'.
                '<div class="col-md-4 p0">'.
                    '<img src="'.$a->thumb.'" class="aktualnosci-img d-block featurette-image mx-auto img-responsive" alt="" >'.
                    '<a class="btn news-btn" href="'. get_site_url().'/galeria/#'.$a->album.'" class="btn btn-inverse" >Przejdź do albumu zdjęć</a>'.
                '</div>'.
            '</div>';
    $l_r=false;
    }else{
        $out.='<div class="row featurette l-rel news"  >'.
                    ' <div class="col-md-4 p0">'.
                         '<img src="'.$a->thumb.'" class="aktualnosci-img d-block featurette-image mx-auto img-responsive" alt="" >'.
                        '<a class="btn news-btn" href="'. get_site_url().'/galeria/#'.$a->album.'" class="btn btn-inverse" >Przejdź do albumu zdjęć</a>'.
                    ' </div>'.
                   ' <div class="col-md-8 ">'.
                        '<h2 class="featurette-heading p10">'.$a->post_title.'</span></h2>'.
                        '<p class="lead p10">'.$a->post_content.'</p>'.
                    '</div>'.
                    
                '</div>';
        $l_r=true;
    }
  //  $out.='<hr class="featurette-divider">';
}

get_header();
include(__DIR__ . "/Elements/StickyMainMenu.php"); ?>
<div style="text-align: center; margin: 35px;" >
    <h1 id="h1-heading" >Aktualności</h1>
</div>
<div id="newsKon" class="container p0" >
    <?php echo $out; ?>
</div>


<?php
get_footer();
?>